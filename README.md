# TRYON-GLASSES PROJECT

The project is consists of client side and python server for marker recognition.

### PYTHON SETUP
uninstall all the opencv versions

```python -m pip uninstall opencv-python --user```

```python -m pip uninstall opencv-contrib-python --user```

after that install opencv-contrib to include sift() and surf() using below given command with python(3.x)

```python -m pip install opencv-contrib-python==3.4.2.16 --user```

also you need to install ```numpy``` and ```matplotlib```.

### SERVER CONTROL
run service:
```bash
systemctl start tryon-glasses
```

restart
```bash
systemctl restart tryon-glasses
```

stop service:
```bash
systemctl stop tryon-glasses
```
check status:
```bash
systemctl status tryon-glasses
```

#### Manual run
From the root folder run
```bash
python3 ./marker-check/server.py
```
