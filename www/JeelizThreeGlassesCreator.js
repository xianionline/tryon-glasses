"use strict";

/**
 * configuration
 * @typedef {Object} AssetsConfig
 * @property {String} envMapURL
 * @property {String} occluderURL
 */
/**
 * @type {AssetsConfig}
 */

/**
 *
 * @param {AssetsConfig} spec
 * @return {Promise<{envMap: THREE.Texture, occluder: THREE.Object3D}>}
 */
const jeelizThreeGlassesCreator=function(spec){
    //enMap texture
    const textureEquirec = new THREE.TextureLoader().load( spec.envMapURL );
    textureEquirec.mapping = THREE.EquirectangularReflectionMapping;
    textureEquirec.magFilter = THREE.LinearFilter;
    textureEquirec.minFilter = THREE.LinearMipMapLinearFilter;

    const occluderLoaded = THREE.JeelizHelper.create_threejsOccluder(spec);

    return occluderLoaded.then(function(loaded) {
        return {
            occluder: loaded,
            envMap: textureEquirec
        }
    })
}
