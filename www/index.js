'use strict'

let scaleCorrectionSlider
let scaleCorrectionSliderValue
let swapCameraButton
let screenshotButton
let calibrateButton
let inputCanvas
let calibrationCanvas
let videoWidth

const ui = {
  controls: {
    markerSelect: null
  },
  info: {
    currentGlassesName: null,
    glassesSize: null,
    markerSizePx: null,
    markerSizeMm: null,
    headWidthPx: null,
    headWidthMm: null,
    headScale: null,
    matchPoints: null,
    calibrationStatus: null,
    mmPerPx: null
  }
}

let faceIsDetected = false

let faceScaleSpan

let THREECAMERA, THREELIGHT
let threeGlasses
let faceObject // main face root object --- it will be rotated and positioned to match face from camera

const MIN_MATCHES = 10

const scaleError = 1.0357142857142856
let scaleCorrection = 0.88
let currentDetectedFaceScale = 1

//const markerDetectionServer = 'https://185.224.139.84:44444/' //for Sezar second server
//const markerDetectionServer = '/' // for python in nearby folder
const markerDetectionServer = 'https://tryon-glasses.happyuser.info:44444/' //for python on remote server

/**
 * markers "database" data
 * @typedef {Object} MarkerData
 * @property {String} key
 * @property {String} title
 * @property {Number} width
 * @property {Number} height
 */
/**
 * @type {MarkerData[]}
 */
let markers = null
let currentMarkerIndex = 0
/**
 * @type {MarkerData}
 */
let currentMarker = null

let glassesWidth = 0 // mm, current glasses width
let currentGlassesIndex = 0
/**
 * glasses "database" data
 * @typedef {Object} GlassesData
 * @property {String} name
 * @property {Array} dimensions
 * @property {String} file
 */
/**
 * @type {GlassesData[]}
 */
let glassesDb = null

let urls = {
  modelsURL: 'models3D/',
  envMapURL: 'envMap.jpg',
  // frameMeshURL: 'branchesBent.glb',
  occluderURL: 'occluder_small.glb'
}

const assets = {
  occluder: null,
  envMap: null,
  glasses: [],
}

let detectedMarkerNameBlock

let assetsLoadPromise

//launched by body.onload() :
function main() {
  const dataLoaded = loadData()

  setupUI()

  // start loading files while setting up everything else
  assetsLoadPromise = loadAssets()

  const codeInitDone = setupVideoCanvas().then(bestVideoSettings => {
    return dataLoaded
      .then(() => {
      return init_faceFilter(bestVideoSettings)
    })
  })

  Promise.all([assetsLoadPromise,codeInitDone]).then(() => {
    console.log('Everything is up...')
    setupGlassesList()

    // TODO later we will switch
    switchToGlasses(currentGlassesIndex)

    setCurrentMarker(ui.controls.markerSelect.value)

    autoCalibrateTick()
  })

  // let supports = navigator.mediaDevices.getSupportedConstraints();
  // if( supports['facingMode'] === true ) {
  //     button.disabled = false;
  //     button.addEventListener('click', function(){
  //         const stream = await navigator.mediaDevices.getUserMedia(opts)
  //     })
  // }
  // console.log('supports[facingMode]', supports['facingMode'])

} //end main()

function loadData() {
  return fetch('data.json').then(result => {
    // console.log('received', result)
    if (!result.ok) {
      reject("HTTP error " + result.status)
      throw new Error("HTTP error " + result.status);
    }
    return result.json();
  }).then((json) => {
    markers = json.markers
    glassesDb = json.glasses
  })
}

let glassesSwitchInProgress = false
function switchToGlasses(index) {
  console.log('Set current glasses to', index)

  // TODO: make some cancelable promises for glasses switch
  if (glassesSwitchInProgress) {
    console.warn('glassesSwitchInProgress', glassesSwitchInProgress)
    return
  }

  if (threeGlasses) {
    faceObject.remove(threeGlasses)
  }

  if (!assets.glasses[index]) {
    assets.glasses[index] = { index: index }
    assets.glasses[index].promise = loadGlasses(index)
  }

  threeGlasses = null
  ui.info.currentGlassesName.innerText = glassesDb[index].name
  ui.info.glassesSize.innerText = glassesDb[index].dimensions[0] + 'x' + glassesDb[index].dimensions[1]
  glassesWidth = glassesDb[index].dimensions[0]

  assets.glasses[index].promise.then(() => {
    faceObject.add(assets.glasses[index].object)
    threeGlasses = assets.glasses[index].object
    threeGlasses.scale.setScalar(scaleCorrection)
  })
}

function loadGlasses(index) {
  return new Promise(resolve => {
    const glassesData = glassesDb[index]

    const loader = new THREE.GLTFLoader().setPath( urls.modelsURL );
    loader.load( glassesData.file, function ( gltf ) {
        console.log('LOADED', gltf.scene );

        const threeGlasses=new THREE.Object3D();
        threeGlasses.name = 'threeGlasses' + index

        const glassesFramesMesh = gltf.scene.getObjectByName('frame')
        console.log('glassesFramesMesh', glassesFramesMesh)
        glassesFramesMesh.traverse( function ( child ) {
            if ( child.isMesh ) {
                child.material.envMap = assets.envMap;
            }
        } );
        threeGlasses.position.copy(glassesFramesMesh.position);
        threeGlasses.add(glassesFramesMesh);

        const glassesLensesMesh = gltf.scene.getObjectByName('lenses')
        glassesLensesMesh.traverse( function ( child ) {
          if ( child.isMesh ) {
            child.material.envMap = assets.envMap;
          }
        } );
        console.log('glassesLensesMesh', glassesLensesMesh)
        // glassesLensesMesh.material = new THREE.MeshBasicMaterial({
        //     envMap: textureEquirec,
        //     opacity: 0.7,
        //     color: 0x2233aa,
        //     transparent: true
        // })
        threeGlasses.add(glassesLensesMesh);

        glassesFramesMesh.position.sub(threeGlasses.position)
        glassesLensesMesh.position.sub(threeGlasses.position)

        assets.glasses[index].object = threeGlasses

        resolve(threeGlasses)
    } );
  })
}

function loadAssets() {
  // Container for glasses
  const threeGlasses=new THREE.Object3D();
  threeGlasses.name = 'threeGlasses'

  return jeelizThreeGlassesCreator(urls).then(function (r) {
    console.log('THEN', r)
    assets.envMap = r.envMap
    assets.occluder = r.occluder
    window.re = r.occluder

    // TODO: load glasses somewhere here
    // assets.glasses.push(r.glasses)
  })
}

function setupVideoCanvas() {
  return new Promise((resolve, reject) => {
    JeelizResizer.size_canvas({
      CSSFlipX: true,
      canvasId: 'jeeFaceFilterCanvas',
      callback: function (isError, bestVideoSettings) {
        //declare ideal values
        const constraints = {
          audio: false,
          video: {
            // width: { ideal: 1280 },
            // height: { ideal: 1024 },
            facingMode: "user"
          }
        };

        console.log('size_canvas.isError', isError)
        // JEEFACEFILTERAPI.get_videoDevices(<function> callback): Should be called before the init method. 2 arguments are provided to the callback function:
        //
        // <array> mediaDevices: an array with all the devices founds. Each device is a javascript object having a deviceId string attribute. This value can be provided to the init method to use a specific webcam. If an error happens, this value is set to false,
        // <string> errorL
        const jeeFaceFilterCanvas = document.getElementById('jeeFaceFilterCanvas')
        jeeFaceFilterCanvas.width = bestVideoSettings.idealWidth
        jeeFaceFilterCanvas.height = bestVideoSettings.idealHeight
        //jeeFaceFilterCanvas.style.zoom = String(bestVideoSettings.idealHeight / bestVideoSettings.idealWidth) // reversed aspect to fit by with

        setupCalibrationVideo(constraints).then(() => {
          JEEFACEFILTERAPI.get_videoDevices((mediaDevices, errorL) => {
            console.log('mediaDevices', mediaDevices)
            console.log('errorL', errorL)
            resolve(bestVideoSettings)
          })
        })
        // JEEFACEFILTERAPI.update_videoElement(<video> vid, <function|False> callback): change the video element used for the face detection (which can be provided via VIDEOSETTINGS.videoElement) by another video element. A callback function can be called when it is done.
      }
    })
  })
}

// callback : launched if a face is detected or lost. TODO : add a cool particle effect WoW !
function detect_callback(faceIndex, isDetected, x, y, z) {
  faceIsDetected = isDetected
  if (isDetected) {
    console.log('INFO in detect_callback() : DETECTED', x, y, z)
  } else {
    console.log('INFO in detect_callback() : LOST', x, y, z)
  }
}

// build the 3D. called once when Jeeliz Face Filter is OK
function init_threeScene(spec) {
  const aspecRatio = spec.canvasElement.width / spec.canvasElement.height
  THREECAMERA = new THREE.PerspectiveCamera(40, aspecRatio, 0.1, 100)

  const threeStuffs = THREE.JeelizHelper.init(spec, detect_callback)
  console.log('threeStuffs', threeStuffs)
  faceObject = threeStuffs.faceObject
  console.log('faceObject', threeStuffs.faceObject)

  assetsLoadPromise.then(() => {
    threeStuffs.faceObject.add(assets.occluder)
  })
} // end init_threeScene()

let calibrateTimeout
function autoCalibrateTick() {
  if (!inputCanvas || !faceIsDetected) {
    if (!faceIsDetected) {
      setCalibrationResultStatus('waiting', 'Face not detected')
    }
    queueNextCalibration()
    return
  }

  calibrateSizes()
    .catch((m) => {
      setCalibrationStatus('error', 'Failed: ' + m)
      console.log('calibration failed', m)
    })
    .finally(() => {
    queueNextCalibration()
  })
}

function setCalibrationResultStatus(status, text = '') {
  const statusesClasses = ['status-error', 'status-done', 'status-waiting']
  const nextClass = 'status-' + status
  if (ui.info.calibrationResultStatus) {
    statusesClasses.forEach(function (v) {
      if (v === nextClass) {
        return
      }
      ui.info.calibrationResultStatus.classList.remove(v)
    })
    ui.info.calibrationResultStatus.classList.add(nextClass)

    ui.info.calibrationResultStatus.innerText = text
  } else {
    console.warn('Calibration Result Status:', status, text)
  }
}

function setCalibrationStatus(status, text = '') {
  const statusesClasses = ['status-working', 'status-error', 'status-done', 'status-waiting']
  const nextClass = 'status-' + status
  if (ui.info.calibrationStatus) {
    statusesClasses.forEach(function (v) {
      if (v === nextClass) {
        return
      }
      ui.info.calibrationStatus.classList.remove(v)
    })
    ui.info.calibrationStatus.classList.add(nextClass)

    ui.info.calibrationStatus.innerText = text
  } else {
    console.warn('Calibration Status:', status, text)
  }
}

function queueNextCalibration() {
  const waitSeconds = 5
  setTimeout(autoCalibrateTick, waitSeconds * 1000)

  setCalibrationStatus('waiting', 'Waiting')

  let timer = waitSeconds
  let intervalId = window.setInterval(() => {
    timer -= .1
    if (timer<=0) {
      timer = 0
      clearInterval(intervalId)
    }
    setCalibrationStatus('waiting', 'Run in ' + timer.toFixed(1) + 's')
  }, 100)
}

//const faceSizeUnits = 1
function setScale(mmPerPx, mpx) {
  let detectedHeadPx = videoWidth * currentDetectedFaceScale
  let detectedHeadMm = mmPerPx * detectedHeadPx

  ui.info.headWidthPx.innerText = detectedHeadPx.toFixed(1)
  ui.info.headWidthMm.innerText = detectedHeadMm.toFixed(1)

  console.log('Face px width', detectedHeadPx)
  // console.log('200h px width', mpx)
  // console.log('200h mm width', mpx / pxToMmRatio)
  // console.warn('ratio', mpx/detectedHeadPx)

  console.log('Face mm width', detectedHeadMm)
  // console.log('ratio mm', 150/detectedHeadMm)

  scaleCorrection = (detectedHeadMm / glassesWidth) * scaleError
  // scaleCorrection = mpx/detectedHeadPx
  console.log('scaleCorrection:', scaleCorrection.toFixed(5))

  console.log('Head px width*', detectedHeadPx * scaleCorrection)

  threeGlasses.scale.setScalar(scaleCorrection)
}

function setupUI() {
  detectedMarkerNameBlock = document.getElementById('detectedMarkerName')
  faceScaleSpan = document.getElementById('faceScaleSpan')

  swapCameraButton = document.getElementById('swapCameraButton')
  screenshotButton = document.getElementById('screenshotButton')

  ui.controls.markerSelect = document.getElementById('marker')

  ui.info.currentGlassesName = document.getElementById('currentGlassesName')
  ui.info.markerSizePx = document.getElementById('markerSizePx')
  ui.info.markerSizeMm = document.getElementById('markerSizeMm')
  ui.info.glassesSize = document.getElementById('glassesSize')
  ui.info.headScale = document.getElementById('headScale')
  ui.info.headWidthMm = document.getElementById('headWidthMm')
  ui.info.headWidthPx = document.getElementById('headWidthPx')
  ui.info.matchPoints = document.getElementById('matchPoints')
  ui.info.calibrationStatus = document.getElementById('calibrationStatusIcon')
  ui.info.calibrationResultStatus = document.getElementById('calibrationResultStatus')
  ui.info.mmPerPx = document.getElementById('mmPerPx')
  ui.info.resultImage = document.getElementById('calibrationResultImage')
  ui.info.calibrationCanvasContainer = document.getElementById('tst-canvas-container')

  ui.controls.markerSelect.addEventListener('change', function(){
    setCurrentMarker(this.value)
  })
}

function toggleCalibrationStats() {
  document.body.classList.toggle('calibrationStats')
}

function setupGlassesList() {
  const list = document.getElementById('glassesList')

  glassesDb.forEach((glassesData, index) => {
    const button = document.createElement('button')
    button.innerText = glassesData.name
    button.style = "    background: url(/models3D/"+glassesData.icon+");\n" +
        "    width: 200px; " +
        "background-repeat: no-repeat;\n" +
        "    height: 75px;\n" +
        "    background-size: 200px;\n" +
        "    border: none;\n" +
        "    color: white;";
    button.addEventListener('click', function() { switchToGlasses(index) })
    list.appendChild(button)
  })
}

function setCurrentMarker(key) {
  currentMarkerIndex = markers.findIndex(function(value) { return value.key === key })
  currentMarker = markers[currentMarkerIndex]

  ui.info.markerSizeMm.innerText = currentMarker.width.toFixed(1) + 'x' + currentMarker.height.toFixed(1)
}

function setupCalibrationVideo(constraints) {
  console.log('setupCalibrationVideo:constraints', constraints)

  return navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
    let $video = document.getElementById('tst-video')
    console.log('getUserMedia---', stream)
    $video.srcObject = stream;
    var strSettings = stream.getVideoTracks()[0].getSettings()
    console.log('strSettings.width', strSettings.width)
    console.log('strSettings.height', strSettings.height)
    console.log('strSettings.aspectRatio', strSettings.aspectRatio)
    console.log('strSettings', strSettings)

    calibrationCanvas = document.createElement('canvas')
    calibrationCanvas.width = strSettings.width
    calibrationCanvas.height = strSettings.height
    if (ui.info.calibrationCanvasContainer) {
      ui.info.calibrationCanvasContainer.appendChild(calibrationCanvas)
    }

    videoWidth = strSettings.width

    // log the real size
    console.log($video, $video.videoWidth, $video.videoHeight);
  }).catch(function(err) {
    console.error('getUserMedia FAILED')
    console.log(err.name + ': ' + err.message);
  });
}

function init_faceFilter(bestVideoSettings) {
  console.log('init_faceFilter:bestVideoSettings', bestVideoSettings)
  return new Promise((resolve, reject) => {
    JEEFACEFILTERAPI.init({
      videoSettings: bestVideoSettings,
      followZRot: true,
      flipX: true,
      canvasId: 'jeeFaceFilterCanvas',
      NNCpath: './dist/', // root of NNC.json file
      maxFacesDetected: 1,
      callbackReady: function (errCode, spec) {
        if (errCode) {
          console.error('AN ERROR HAPPENS. ERR =', errCode)
          reject(errCode)
          return
        }

        console.log('INFO : JEEFACEFILTERAPI IS READY', spec)
        init_threeScene(spec)

        inputCanvas = spec.canvasElement
        resolve()
      }, //end callbackReady()
      //called at each render iteration (drawing loop) :
      callbackTrack: function (detectState) {
        if (!faceObject) {
          console.log('wait...')
          return
        }

        //console.log('callbackTrack', detectState.s)
        if (currentDetectedFaceScale !== detectState.s) {
          ui.info.headScale.innerText = detectState.s.toFixed(3)
          ui.info.headWidthPx.innerText = (videoWidth * currentDetectedFaceScale).toFixed(1)

          currentDetectedFaceScale = detectState.s
        }
        //detectState.s *= scaleCorrection
        THREE.JeelizHelper.render(detectState, THREECAMERA, THREELIGHT)

        // if (!occluderNew && window.re.parent) {
        //   occluderNew = true
        //   const loader = new THREE.GLTFLoader().setPath('models3D/');
        //   loader.load('occluderNew.glb', function (gltf) {
        //     occluderNew = gltf.scene.getObjectByName('occluder')
        //     occluderNew.material.wireframe = true
        //
        //     //occluderNew.rotation.x += Math.PI/2
        //     //occluderNew.position.set(0, 0.1, -0.04)
        //     //occluderNew.scale.multiplyScalar(0.0084)
        //     window.reNew = occluderNew
        //     console.log('XXXXX', window.re.parent === threeGlasses.parent)
        //     /*
        //      0.004448 m
        //     -0.564062 m
        //      0.245886 m
        //      */
        //     const sg = new THREE.SphereBufferGeometry(0.01,8,6)
        //     var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
        //     var sphere = new THREE.Mesh( sg, material );
        //     sphere.position.set( 0.004448, 0.345886, 0.524062)
        //     window.re.parent.add( sphere );
        //
        //     window.re.parent.add(occluderNew);
        //   });
        // }

      } //end callbackTrack()
    }) //end JEEFACEFILTERAPI.init call
  })
}

function b64ToUint8Array(b64Image) {
  var img = atob(b64Image.split(',')[1])
  var img_buffer = []
  var i = 0
  while (i < img.length) {
    img_buffer.push(img.charCodeAt(i))
    i++
  }
  return new Uint8Array(img_buffer)
}

function calibrateSizes() {
  return new Promise((resolve, reject) => {
    if (!inputCanvas) {
      console.error('input canvas is not available!')
      reject('input canvas is not available!')
      return
    }

    var $video = document.getElementById('tst-video')
    calibrationCanvas.width = $video.videoWidth
    calibrationCanvas.height = $video.videoHeight
    calibrationCanvas.getContext('2d')
      .drawImage($video, 0, 0, calibrationCanvas.width, calibrationCanvas.height);

    const b64Image = calibrationCanvas.toDataURL('image/jpeg')
    //const b64Image = inputCanvas.toDataURL('image/jpeg')
    const u8Image = b64ToUint8Array(b64Image)

    const file = new Blob([u8Image], { type: 'image/jpg' })
    file.lastModifiedDate = new Date()
    file.name = 'tmp.jpg'

    const data = new FormData()
    data.append('file', file, file.name)
    if (!document.getElementById("autoDetectMarker").checked) {
      data.append('marker', currentMarker.key)
    }

    setCalibrationStatus('working', 'Sending...')

    fetch(markerDetectionServer, {
      method: 'POST',
      body: data
    }).then(result => {
      // console.log('received', result)
      if (!result.ok) {
        reject("HTTP error " + result.status)
        throw new Error("HTTP error " + result.status);
      }
      return result.json();
    }).then((json) => {
      // console.log('received json', json)

      if (!json.width || !json.height) {
        reject()
        console.warn('marker not detected')
        setCalibrationResultStatus('waiting', 'No match')
        return
      }

      setCalibrationResultStatus('done', json.matches > MIN_MATCHES? 'OK' : 'Poor match')
      setCalibrationStatus('done', 'OK')

      console.log('calibrateSizes = 2 ========')

      if (json.marker) {
        console.log('Detected marker filename', json.marker)
        if (json.marker !== currentMarker.key) {
          currentMarker = markers.find(m => m.key === json.marker)
          console.log('Switched to detected marker', currentMarker)
        }
      }

      if (json.features) {
        ui.info.matchPoints.innerText = json.matches + ' / ' + json.features + ' (' + (json.matches/json.features).toFixed(2) +  ')'
      } else {
        ui.info.matchPoints.innerText = json.matches
      }

      if (ui.info.resultImage) {
        ui.info.resultImage.src = markerDetectionServer + json.img
      }

      if (json.matches < MIN_MATCHES) {
        return
      }

      // console.log('currentMarker', currentMarker, ':', markers[currentMarker])
      let mmPerPx = currentMarker.width / json.width

      ui.info.markerSizePx.innerText = json.width.toFixed(1) + 'x' + json.height.toFixed(1)

      console.log('markers[currentMarker].width / json.width', currentMarker.width.toFixed(4), json.width.toFixed(4))
      console.log('mmPerPx', mmPerPx.toFixed(4))

      ui.info.mmPerPx.innerText = mmPerPx.toFixed(3)

      // if (!(window.re.material instanceof THREE.MeshPhongMaterial)) {
      //   window.re.material = new THREE.MeshPhongMaterial({ color: '#0f0', wireframe: true })
      // }

      setScale(mmPerPx, json.width)
    }).finally(() => {
      resolve()
    })
  })
}
