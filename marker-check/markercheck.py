import marker
import os
import json
import time
import cv2

#----------------------------------------------------------------------
def remove(path):
    """
    Remove the file or directory
    """
    if os.path.isdir(path):
        try:
            os.rmdir(path)
        except OSError:
            print ("Unable to remove folder: %s" % path)
    else:
        try:
            if os.path.exists(path):
                os.remove(path)
        except OSError:
            print ("Unable to remove file: %s" % path)
 
#----------------------------------------------------------------------
def cleanup(number_of_minutes, path):
    """
    Removes files from the passed in path that are older than or equal 
    to the number_of_minutes
    """
    time_in_secs = time.time() - (number_of_minutes * 60)
    for root, dirs, files in os.walk(path, topdown=False):
        for file_ in files:
            full_path = os.path.join(root, file_)
            stat = os.stat(full_path)
 
            if stat.st_mtime <= time_in_secs:
                remove(full_path)
 
        #if not os.listdir(root):
        #    remove(root)

def detectMatch(testImagePath, markerName):
    cleanup(3, "./www/results/")
    imgName = "./results/match_result_" + str(time.time()) + ".jpg"
    bestMatch = { "points": 0, "w": 0, "h": 0, "resultImage":"", "markerFile":"" }
    bestMatchResultImage = None
    minMatches = 10
    m = '../markers/' + markerName + '.jpg'

    testImage = cv2.imread(testImagePath,cv2.IMREAD_GRAYSCALE) # test image
    markerImage = cv2.imread(m,cv2.IMREAD_GRAYSCALE)          # query image

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    kp2, des2 = sift.detectAndCompute(testImage,None) #test image
    kp1, des1 = sift.detectAndCompute(markerImage,None)

    w, h, goodMatches, img3, features = marker.detectAndGetSizeSift(markerImage, testImage, kp1, des1, kp2, des2, minMatches)
    if (goodMatches > 0):
        bestMatch['markerFile'] = markerName
        bestMatchResultImage = img3
        bestMatch['points'] = goodMatches
        bestMatch['features'] = features
        bestMatch['w'] = w
        bestMatch['h'] = h
        print("BM-W",w)
    
    if bestMatchResultImage is not None:
        cv2.imwrite(imgName, bestMatchResultImage)
        bestMatch['resultImage'] = imgName

    return bestMatch


def detectBestMatch(testImagePath):
    cleanup(3, "./www/results/")
    imgName = "./results/match_result_" + str(time.time()) + ".jpg"
    bestMatch = { "points": 0, "w": 0, "h": 0, "resultImage":"", "markerFile":"" }
    bestMatchResultImage = None
    minMatches = 10

    #print("test:", testImagePath)
    testImage = cv2.imread(testImagePath,cv2.IMREAD_GRAYSCALE) # test image
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # TODO: cach this kp and des in marker check and just pass here
    kp2, des2 = sift.detectAndCompute(testImage,None) #test image

    markers = []
    #try:
    with open('./data.json') as json_data_file:
        dataConfig = json.load(json_data_file)
        markers = dataConfig["markers"]
    # except:
    #     print("failed to open markers config")
    #     dirpath = os.getcwd()
    #     print("current directory is : " + dirpath)


    for markerData in markers:
        m = "../markers/" + markerData["key"] + ".jpg"
        #print("   against marker:", m)
        try:
            f = open(m)
            f.close()
        except IOError:
            f.close()
            #print("   !!! MARKER FILE NOT FOUND! - skipped (" + m + ")")
            continue
        
        #fileData = form["file"].file.read()
        markerImage = cv2.imread(m,cv2.IMREAD_GRAYSCALE)          # query image
    
        # find the keypoints and descriptors with SIFT
        # TODO: cach this kp and des in file or memory
        # TODO: measure time for this line
        kp1, des1 = sift.detectAndCompute(markerImage,None)

        w, h, goodMatches, img3, features = marker.detectAndGetSizeSift(markerImage, testImage, kp1, des1, kp2, des2, minMatches)
        if (goodMatches > 0 and goodMatches > bestMatch['points']):
            bestMatch['markerFile'] = markerData["key"]
            bestMatchResultImage = img3
            bestMatch['points'] = goodMatches
            bestMatch['features'] = features
            bestMatch['w'] = w
            bestMatch['h'] = h
    
    if bestMatchResultImage is not None:
        cv2.imwrite(imgName, bestMatchResultImage)
        bestMatch['resultImage'] = imgName

    return bestMatch
