#!/usr/env python3
import math
import numpy as np
import cv2
from matplotlib import pyplot as plt

def euclidianDistance(p1,p2):
    return math.sqrt( distanceToSquared( p1,p2 ) )

def distanceToSquared(p1,p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return dx * dx + dy * dy

MIN_MATCH_COUNT = 10

def detectAndGetSize2(markerImagePath, testImage):
    print("getSize2")
    img1 = cv2.imread(markerImagePath,cv2.IMREAD_GRAYSCALE)          # query image

    # test image
    #testImage.seek(0)
    bytes_as_np_array = np.asarray(bytearray(testImage.read()), dtype=np.uint8)
    #bytes_as_np_array = np.frombuffer(testImage, dtype=np.uint8)
    print("getSize2:read done")
    img2 = cv2.imdecode(bytes_as_np_array,cv2.IMREAD_GRAYSCALE)
    print("getSize2:decode done")

    return detectAndGetSizeImg(img1, img2)

def detectAndGetSize(markerImagePath, testImagePath, minMatch = MIN_MATCH_COUNT):
    img1 = cv2.imread(markerImagePath,cv2.IMREAD_GRAYSCALE)          # query image
    img2 = cv2.imread(testImagePath,cv2.IMREAD_GRAYSCALE) # test image
    return detectAndGetSizeImg(img1, img2, minMatch)

def detectAndGetSizeImg(img1, img2, minMatch = MIN_MATCH_COUNT):
    print("getSizeImg")
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    # TODO: cach this kp and des in file or memory
    # TODO: measure time for this line
    kp1, des1 = sift.detectAndCompute(img1,None)
    
    # TODO: cach this kp and des in marker check and just pass here
    kp2, des2 = sift.detectAndCompute(img2,None) #test image
    return detectAndGetSizeSift(img1, img2, kp1, des1, kp2, des2)

def detectAndGetSizeSift(img1, img2, kp1, des1, kp2, des2, minMatch = MIN_MATCH_COUNT):

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    if False:
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(des1,des2,k=2)
    else:
        # BFMatcher with default params
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1,des2,k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.6*n.distance:
            good.append(m)

    if len(good)>=minMatch:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()

        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)

        img2 = cv2.polylines(img2,[np.int32(dst)],True,255,12, cv2.LINE_AA)
        img2 = cv2.polylines(img2,[np.int32(dst)],True,(0,0,0),3, cv2.LINE_AA)
    
        
        d1 = euclidianDistance(dst[0][0],dst[3][0])
        d2 = euclidianDistance(dst[0][0],dst[1][0])
        d3 = euclidianDistance(dst[1][0],dst[2][0])
        d4 = euclidianDistance(dst[2][0],dst[3][0])

        dw = (d1 + d3) / 2
        dh = (d2 + d4) / 2

        #print("Px Lengths are %d, %d, %d, %d " %( d1,d2,d3,d4 ))
        #print("Px Mid H,W are %d, %d " %( dw,dh ))

        draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                        singlePointColor = None,
                        matchesMask = matchesMask, # draw only inliers
                        flags = 2)

        img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
        return (dw,dh,len(good),img3, len(des1))
    else:
        #print("Not enough matches are found - %d/%d " %(len(good),minMatch))
        matchesMask = None

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                    singlePointColor = None,
                    matchesMask = matchesMask, # draw only inliers
                    flags = 2)

    img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    return (0,0,len(good),img3, len(des1))
